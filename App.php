<?php
    
    namespace plugin\test;

    defined( 'ABSPATH' ) || exit;

    use plugin\test\src\classes\PostType;
    use plugin\test\src\classes\RestAPI;
    use plugin\test\src\classes\ShortCode;
    use plugin\test\src\classes\Widget;
    use plugin\test\src\menu;

    class App
    {

        public static function config(): array
        {
            global $title;
            return [
            
                'title'     => $title,
                'description' => 'This project was created for test.',
                'version'   => '1.0.0'
            ];
        }

        public static function init(): void
        {

            /*
            |--------------------------------------------------------------------------
            | Create post type
            |--------------------------------------------------------------------------
            */
            $postType = new PostType( 'movies' );
            add_action( 'init', [ $postType, 'create'] );
            register_activation_hook( __FILE__, [ $postType, 'flushRewrites'] );
            register_uninstall_hook( __FILE__, 'PostType::deactivate' );

            /*
            |--------------------------------------------------------------------------
            | Create widget
            |--------------------------------------------------------------------------
            */
            new Widget();

            /*
            |--------------------------------------------------------------------------
            | Get data by restAPI in "/api/v1/movies"
            |--------------------------------------------------------------------------
            */
            new RestAPI();

            /*
            |--------------------------------------------------------------------------
            | Get data by shortcode "plugin_test"
            |--------------------------------------------------------------------------
            */
            new ShortCode();

            /*
            |--------------------------------------------------------------------------
            | Get data in a separated page
            |--------------------------------------------------------------------------
            */
            new menu();

        }
    }