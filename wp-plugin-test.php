<?php
    /*
    Plugin Name: WP-Plugin-Test
    Description: This plugin was created for test..
    Author: Saeed Modarresi
    Author URI: https://saeedmodarresi.com
    Version: 1.0.0
    WP Tested up to: 5.9.2
    License:  GPLv3
    License URI: https://www.gnu.org/licenses/gpl-3.0.html
    */

    defined( 'ABSPATH' ) || exit;


    /*
    |--------------------------------------------------------------------------
    | Autoload
    |--------------------------------------------------------------------------
    */
    require_once __DIR__ . "/vendor/autoload.php";


    /*
    |--------------------------------------------------------------------------
    | Create and Run Application
    |--------------------------------------------------------------------------
    */
    if ( ! plugin\test\App::class ) return;
    \plugin\test\App::init();