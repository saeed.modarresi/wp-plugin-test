<?php

    namespace plugin\test\src\classes;

    use plugin\test\src\base;

    class Template extends base {

        public function __construct()
        {
            $data = $this->getData();
            return $this->view('movies', $data);
        }
    }