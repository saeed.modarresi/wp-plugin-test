<?php

    namespace plugin\test\src\classes;

    use plugin\test\src\base;

    class ShortCode extends base {

        public function __construct()
        {
            add_shortcode('plugin_test', function (){

                return json_encode($this->getData());
            });
        }

    }