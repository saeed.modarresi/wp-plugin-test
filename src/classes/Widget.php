<?php

    namespace plugin\test\src\classes;

    class Widget extends \WP_Widget
    {

        public $args = [
            'before_title'  => '<h4 class="widgettitle">',
            'after_title'   => '</h4>',
            'before_widget' => '<div class="widget-wrap">',
            'after_widget'  => '</div></div>'
        ];

        public function __construct( ) {

            $title = 'movies';
            parent::__construct( $title, ucfirst($title) );

            add_action( 'widgets_init', function() {
                register_widget( 'plugin\test\src\classes\Widget' );
            });
        }

        public function widget( $args, $instance ) {

            echo $args['before_widget'];
            if ( ! empty( $instance['title'] ) ) {
                echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
            }
            echo '<div class="textwidget">';
            echo esc_html__( $instance['text'], 'text_domain' );
            echo '</div>';
            echo $args['after_widget'];

        }

        public function form( $instance ) {

            $feed  = new WP_Query( ['post_type' => 'movies'] );
            $count = $feed->found_posts;
            ?>
            <h6>Count of movies: <?php echo !empty($count) ? $count : 0 ?></h6>
            <?php

        }
    }