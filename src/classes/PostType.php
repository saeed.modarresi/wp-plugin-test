<?php

    namespace plugin\test\src\classes;

    class PostType{

        public $customPostType;
        public static $deactivate;
        public $customPostTypeCat;
        public $customPostTypeTag;

        /*
        |--------------------------------------------------------------------------
        | Activate plugin
        |--------------------------------------------------------------------------
        */
        public function __construct( $title )
        {
            $this->customPostType    = $title;
            $this->customPostTypeCat = "category-{$title}";
            $this->customPostTypeTag = "tag-{$title}";
            Self::$deactivate = $title;
        }

        /*
        |--------------------------------------------------------------------------
        | Activate plugin
        |--------------------------------------------------------------------------
        */
        public function create()
        {
            $this->register();
            $this->categories();
            $this->tags();
        }

        /*
        |--------------------------------------------------------------------------
        | Create post type
        |--------------------------------------------------------------------------
        */
        public function register()
        {
            $supports = [
                'title',
                'editor',
                'excerpt',
                'thumbnail'
            ];

            $argsMovies = [
                'label'           => ucfirst($this->customPostType),
                'public'          => true,
                'show_ui'         => true,
                'capability_type' => 'post',
                'hierarchical'    => false,
                'menu_icon'       => 'dashicons-format-video',
                'rewrite'         => [ 'slug' => $this->customPostType, 'with_front' => false ],
                'query_var'       => true,
                'supports'        => $supports
            ];

            register_post_type( $this->customPostType, $argsMovies);
        }

        /*
        |--------------------------------------------------------------------------
        | Create post type categories
        |--------------------------------------------------------------------------
        */
        public function categories()
        {
            $argsTaxMovies = [
                'label'             => ucfirst(str_replace('-',' ',$this->customPostTypeCat)),
                'public'            => TRUE,
                'hierarchical'      => TRUE,
                'show_ui'           => TRUE,
                'show_in_nav_menus' => TRUE,
                'args'              => [ 'orderby' => 'term_order' ],
                'rewrite'           => [ 'slug' => $this->customPostTypeCat, 'with_front' => false ],
                'query_var'         => TRUE
            ];
            register_taxonomy( $this->customPostTypeCat, $this->customPostType, $argsTaxMovies );
        }

        /*
        |--------------------------------------------------------------------------
        | Create post type tags
        |--------------------------------------------------------------------------
        */
        public function tags()
        {
            $argsTagMovies = [
                'label'             => ucfirst(str_replace('-',' ', $this->customPostTypeTag)),
                'public'            => TRUE,
                'hierarchical'      => FALSE,
                'show_ui'           => TRUE,
                'show_in_nav_menus' => TRUE,
                'args'              => [ 'orderby' => 'term_order' ],
                'rewrite'           => [ 'slug' => $this->customPostTypeTag, 'with_front' => TRUE ],
                'query_var'         => TRUE
            ];
            register_taxonomy( $this->customPostTypeTag, $this->customPostType, $argsTagMovies );
        }

        /*
        |--------------------------------------------------------------------------
        | Flush rewrite
        |--------------------------------------------------------------------------
        */
        public function flushRewrites()
        {
            $this->create();
            flush_rewrite_rules();
        }

        /*
        |--------------------------------------------------------------------------
        | Deactivate plugin
        |--------------------------------------------------------------------------
        */
        public static function deactivate()
        {
            unregister_post_type( Self::$deactivate );
        }
    }