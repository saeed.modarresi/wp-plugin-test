<?php

    namespace plugin\test\src\classes;

    use plugin\test\src\base;

    class RestAPI extends base {

        public function __construct()
        {
            add_action('parse_request',function (){
                return $this->parser();
            });
        }

        public function parser()
        {
            $route = $_SERVER['REQUEST_URI'];

            $routes = [
                '/api/v1/movies' => 'showDataByRestAPI'
            ];

            foreach ($routes as $key => $value){

                if( str_contains($route,$key) ) call_user_func( $this->$value() );
            }

        }

        public function showDataByRestAPI()
        {
            return wp_send_json($this->getData());
        }
    }