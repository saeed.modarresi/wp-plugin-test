<?php

    namespace plugin\test\src;

    use plugin\test\src\classes\Template;

    class menu extends base
    {
        public function __construct()
        {
            add_action( 'admin_menu', function (){
                $this->registerMenu();
            });
        }

        public function registerMenu()
        {
            add_menu_page(
                'WP Plugin Test',
                'WP Plugin Test',
                'manage_options',
                'plugin-test',
                [$this,'init'],
                'dashicons-admin-post',
                50
            );
        }

        public static function init()
        {
            $data = self::getData();
            require (__DIR__ .'/../templates/movies.php');
        }
    }