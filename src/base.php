<?php

    namespace plugin\test\src;

    class base{

        public function getData()
        {

            $args = [
                'post_type'   => 'movies',
                'post_status' => 'publish',
                'nopaging' => true
            ];

            $feed   = new \WP_Query( $args );

            $output = [];

            while ($feed->have_posts()) {

                $feed->the_post();

                $terms = $this->getTerms(get_the_ID());

                $output[] = [
                    'id'            => get_the_ID(),
                    'title'         => get_the_title(),
                    'images'        => get_the_post_thumbnail_url(),
                    'description'   => get_the_content(),
                    'excerpt'       => get_the_excerpt(),
                    'categories'    => !empty($terms['categories']) ? $terms['categories'] : '',
                    'tags'          => !empty($terms['tags']) ? $terms['tags'] : '',
                ];
            }
            wp_reset_postdata();

            return $output;
        }

        public function getTerms( $id )
        {
            global $wpdb;

            $table1  = $wpdb->prefix . 'term_relationships';
            $table2  = $wpdb->prefix . 'term_taxonomy';
            $table3  = $wpdb->prefix . 'terms';
            $result  = $wpdb->get_results(
                "SELECT * FROM {$table1} 
                                  LEFT JOIN {$table2} ON {$table1}.term_taxonomy_id = {$table2}.term_id 
                                  LEFT JOIN {$table3} ON {$table2}.term_id = {$table3}.term_id 
                                  WHERE object_id = '{$id}'"
            );

            $terms = [];

            foreach ( $result as $item ) {

                if ( str_contains($item->taxonomy, 'category') ) $terms['categories'][] = $item;
                if ( str_contains($item->taxonomy, 'tag') ) $terms['tags'][] = $item;
            }

            return $terms;
        }

    }