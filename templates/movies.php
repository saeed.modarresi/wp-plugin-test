<?php
    if (!empty($data)) {
        ?>
        <div class="movies">
            <?php
            foreach ($data as $datum){
                ?>
                <div>
                    <img src="<?php echo !empty($datum['images']) ? $datum['images'] : '' ?>" alt="">
                    <h3><?php echo !empty($datum['title']) ? $datum['title'] : '' ?></h3>
                    <p><?php echo !empty($datum['excerpt']) ? $datum['excerpt'] : '' ?></p>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
?>